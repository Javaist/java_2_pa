package service;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import com.google.gson.Gson;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import models.Gender;
import models.LikeState;
import models.User;
import modelsForRandomUserImport.RandomUserList;
import modelsForRandomUserImport.Result;
/**
 * Load UserList from URL
 * User-URL: https://randomuser.me/api/?nat=de
 * model-Klassen über http://www.jsonschema2pojo.org erstellen lassen
 * @author Ben
 *
 */
public class LoadUserListService extends Service<List<User>>{
	private String url;
	
	public LoadUserListService() {
	}
	
	public LoadUserListService(String url) {
		this.url = url;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@Override
	protected Task<List<User>> createTask() {
		Task<List<User>> task = new Task<List<User>>() {
			
			ArrayList<User> convertedUserList = new ArrayList<>();

			@Override
			protected List<User> call() throws Exception {
				RandomUserList userList;
				InputStream input = new URL(url).openStream();
				Reader json = new InputStreamReader(input, StandardCharsets.UTF_8);
				Gson gson = new Gson();
						
				userList = gson.fromJson(json, RandomUserList.class);
				
				for (Result user : userList.results) {
	    			
	    			User newUser = new User();
	    			
	    			switch(user.gender) {
	    				case "male": newUser.setGender(Gender.MALE);
	    					break;
	    				case "female": newUser.setGender(Gender.FEMALE);
	    			}
	    			
	    			newUser.setFirstname(user.name.first);
	    			newUser.setLastname(user.name.last); 
	    			newUser.setStreet(user.location.street.name + " " + user.location.street.number);  // Straße + Hausnummer zusammengefasst
	    			newUser.setCity(user.location.city);
	    			newUser.setState(user.location.state);
	    			newUser.setPostcode(user.location.postcode);  
	    			newUser.setEmail(user.email); 
	    			newUser.setPhone(user.cell);
	    			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ROOT);
	    			LocalDateTime parsedDate = LocalDateTime.parse(user.dob.date, formatter);
	    			newUser.setDob(parsedDate); // sql.Timestamp zu LocalDateTime
	    			newUser.setImgURL(user.picture.large);
	    			newUser.setLikes(new HashMap<Integer, LikeState>()); // leere Map für "Likes" mitgeben
	    			
	    			convertedUserList.add(newUser);
				
				}
				
				return convertedUserList;
			}
		};
		return task;
	}

}
