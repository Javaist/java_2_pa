package service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import db.DBConnection;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

/*
 * Serverabfrage: Wie viele Datensätze vorhanden sind
 */

public class CheckServerCountService extends Service<Integer>{
	
	
	@Override
	protected Task<Integer> createTask() {
		Task<Integer> task = new Task<Integer>() {
			int count = 0;
			
			@Override
			protected Integer call() throws Exception {
				Connection con = DBConnection.getInstance().connection();
				
				try {
					Statement selectStatement = con.createStatement();
					ResultSet rs = selectStatement.executeQuery("SELECT COUNT(*) FROM userlist");
					rs.next();
					count = rs.getInt(1);
				} catch (SQLException e) {
					System.out.println("Fehler beim Auslesen der Anzahl der Datensätze");
					e.printStackTrace();
				}
				
				return count;
			}
		};
		
		return task;
	}

}
