package service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import com.google.gson.Gson;

import db.DBConnection;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import models.User;

/**
 * load random UserList to Server
 * @author Ben
 *
 */
public class SaveUserListToServerService extends Service<Boolean> {
	
	private List<User> userList;
	
	public List<User> getuserList() {
		return userList;
	}

	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	@Override
	protected Task<Boolean> createTask() {
		Task<Boolean> task = new Task<Boolean>() {

			@Override
			protected Boolean call() throws Exception {
				
					Connection con = DBConnection.getInstance().connection();

					String sql = "INSERT INTO userlist (gender, firstname, lastname, street, city, state, postcode, email, phone, dob, picture, likes) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
					try (PreparedStatement ps = con.prepareStatement(sql)){  // try with resource; bietet Auto-close() (Autoclosable), wenn in try (!!!) 
						for (User user : userList) {
							// user.getLikes --> HashMap in JSON konvertieren mit GSON
							Gson gson = new Gson();
							String likesJson = gson.toJson(user.getLikes());
							
							ps.setString(1, user.getGender().name());  // .name(): enum konvertieren quasi toString
							ps.setString(2, user.getFirstname());
							ps.setString(3, user.getLastname()); 
							ps.setString(4, user.getStreet());
							ps.setString(5, user.getCity());
							ps.setString(6, user.getState());
							ps.setInt(7, user.getPostcode());  
							ps.setString(8, user.getEmail()); 
							ps.setString(9, user.getPhone());
							ps.setString(10, user.getDob().toString());  // LocalDateTime toString
							ps.setString(11, user.getImgURL());
							ps.setString(12, likesJson);
							
							ps.addBatch();
						}
						
						ps.executeBatch();
						
						return ps.getUpdateCount() == 1;
						
					} catch (SQLException e) {
						e.printStackTrace();
					}
				
				return null;
			}
			
		};
		return task;
	}
	

}
