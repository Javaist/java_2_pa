package models;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;

import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;

public class User {

	private int id;
	private Gender gender;
	private String firstname;
	private String lastname;
	private String street;   // streetname + streetnumber
	private String city;
	private String state;
	private int postcode;
	private String email;
	private String phone;
	private LocalDateTime dob;
	private String imgURL;
	private HashMap<Integer, LikeState> likes;
	private Button btn;
	
	public User() {
		//leerer Kontruktor
	}

	public User(int id, Gender gender, String firstname, String lastname, String street, String city, String state, int postcode, String email, String phone, LocalDateTime dob,
			String imgURL, HashMap<Integer, LikeState> likes, Button btn) {
		this.id = id;
		this.gender = gender;
		this.firstname = firstname;
		this.lastname = lastname;
		this.street = street;
		this.city = city;
		this.state = state;
		this.postcode = postcode;
		this.email = email;
		this.phone = phone;
		this.dob = dob;
		this.imgURL = imgURL;
		this.likes = likes;
		this.btn = btn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Gender getGender() {
		return gender;
	}

	public void setGender(Gender gender) {
		this.gender = gender;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String streetname) {
		this.street = streetname;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public int getPostcode() {
		return postcode;
	}

	public void setPostcode(int postcode) {
		this.postcode = postcode;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public LocalDateTime getDob() {
		return dob;
	}

	public void setDob(LocalDateTime dob) {
		this.dob = dob;
	}

	public String getImgURL() {
		return imgURL;
	}

	public void setImgURL(String imgURL) {
		this.imgURL = imgURL;
	}

	public HashMap<Integer, LikeState> getLikes() {
		return likes;
	}

	public void setLikes(HashMap<Integer, LikeState> likes) {
		this.likes = likes;
	}
	
	public int getAge() {
		int age = (int) ChronoUnit.YEARS.between(dob, LocalDateTime.now());
		return age;
	}

	public Button getBtn() {
		return btn;
	}

	public void setBtn(Button btn) {
		this.btn = btn;
	}
	
	public StackPane getImg() {
		Image img = new Image(imgURL, 80, 80, true, true);
		ImageView imgView = new ImageView(img);
		imgView.getStyleClass().add("imageView"); 
		StackPane pane = new StackPane(imgView);  // ImgView in StackPane, damit Boder per CSS möglich ist
		pane.setMaxWidth(80);
		pane.getStyleClass().add("panImg-border");
		return pane;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", gender=" + gender + ", firstname=" + firstname + ", likes=" + likes + "]";
	}
	
}
