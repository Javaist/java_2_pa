package application;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import dao.MySQLUserDAO;
import dao.UserDAO;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import models.Gender;
import models.LikeState;
import models.User;
import service.CheckServerCountService;
import service.LoadUserListService;
import service.SaveUserListToServerService;

public class FXTinderController {
	
    @FXML
    private ImageView imageViewTinder;

    @FXML
    private Label labelUserTinder;

    @FXML
    private Button dislikeBtn;

    @FXML
    private Button likeBtn;
    
    @FXML
    private Tab favTab;

    @FXML
    private ToggleGroup gender;
    
    @FXML
    private RadioButton female;

    @FXML
    private RadioButton male;
    
    @FXML
    private TableView<User> favTableView;
    
    @FXML
    private TableColumn<User, String> imgCol;

    @FXML
    private TableColumn<User, String> nameCol;

    @FXML
    private TableColumn<User, String> ageCol;
    
    @FXML
    private TableColumn<User, String> btnCol;
    
    @FXML
    private Tab matchesTab;
    
    @FXML
    private ImageView imageViewMatches;

    @FXML
    private Label labelUserMatches;
    
    @FXML
    private Label labelMatchesCount;

    @FXML
    private Label labelFirstname;

    @FXML
    private Label labelLastname;

    @FXML
    private Label labelStreet;

    @FXML
    private Label labelPostcode;

    @FXML
    private Label labelCity;

    @FXML
    private Label labelEmail;

    @FXML
    private Label labelPhone;
    
    @FXML
    private Button btnNext;
    
    @FXML
    private ImageView imageViewYourProfile;

    @FXML
    private Label labelYourFirstname;

    @FXML
    private Label labelYourLastname;

    @FXML
    private Label labelYourStreet;

    @FXML
    private Label labelYourPostcode;

    @FXML
    private Label labelYourCity;

    @FXML
    private Label labelYourEmail;

    @FXML
    private Label labelYourPhone;
    
    // randomUser
    private String url = "https://randomuser.me/api/?nat=de&results=500";
    private CheckServerCountService count = new CheckServerCountService();
    private LoadUserListService loadUserList = new LoadUserListService();
    private SaveUserListToServerService saveUserToServer = new SaveUserListToServerService();
    
    private UserDAO dao;
    private List<User> userListDB;		// Userliste für User aus Datenbank / von Server
    private User activeUser; // that's you
    private User currentUserProfile;
    private Gender genderChoice = Gender.FEMALE;  // same as Default by RadioButton
    private Random rg = new Random();
    private ArrayList<User> matchesList;
    private int matchId;
    
    ///////// Methoden ///////////
    @FXML
    void initialize() {
    	
    	// Grafiken auf die Buttons für "Like" und "Dislike" setzen
    	dislikeBtn.setGraphic(new ImageView("img/dislikeBtn_03.png"));
    	likeBtn.setGraphic(new ImageView("img/likeBtn_03.png"));

    	/// Server/DB mit RandomDaten befüllen, falls noch leer
    	count.restart();
    	count.setOnSucceeded(e -> {
    		// wenn DB noch leer, dann füllen, sonst nicht
    		if (count.getValue() < 1) { 	
	    		loadUserList.setUrl(url);
		    	loadUserList.restart();
		    	loadUserList.setOnSucceeded(event -> {
		    		List<User> userList = loadUserList.getValue();
		    		System.out.println("Anzahl User in Random-Liste: " + userList.size());
					saveUserToServer.setUserList(userList);
					saveUserToServer.restart();
		    		saveUserToServer.setOnSucceeded(eve -> {
		    			startFXStage();
		    		});
		    	});
    		} else {
    			startFXStage();
    		}
    	});
    }

	private void startFXStage() {
		try {
//			dao = new DummyUserDAO();
    		dao = new MySQLUserDAO();  // TODO
			
    		userListDB = dao.findAll();
    		
			activeUser = userListDB.get(0);  // erster Datensatz als aktiven Nutzer angenommen/festlegen
			loadNextUserProfile();
			
		} catch (Exception ex) {
			System.out.println("Fehler: " + ex);
		}
		
		// FavoritenTab
		setupTableView();
		setupContextMenu();
		updateFavTableView();
		
		// ProfileTab
		setupProfileTab();
		
//		errorHandling();
	}

	private void loadNextUserProfile() {
		
		ArrayList<User> filteredUserList = new ArrayList<>();
		
		for (User user : userListDB) {
			if(user.getGender().equals(genderChoice) 		// prüfen, ob Geschlecht des aktuellen Userprofils der Auswahl entspricht (RadioButton)
					&& !user.equals(activeUser) 			// und ob aktuelles Userprofil nicht gleich dem aktiven Anwender ist
					&& !user.getLikes().containsKey(activeUser.getId())) {  // und ob aktuelles Userprofil noch kein "Like" vom aktiven Anwender erhalten hat
				
				filteredUserList.add(user);
			}
		}
		
		if (filteredUserList.isEmpty()) {
//			System.out.println("Keine passenden Profile mehr vorhanden.");
			dislikeBtn.setDisable(true);
			likeBtn.setDisable(true);
			Alert a = new Alert(AlertType.NONE, "Du hast alle Profile bewertet.", ButtonType.OK);
			a.show();
		} else {
			currentUserProfile = filteredUserList.get(0);
//			System.out.println(filteredUserList.get(0).toString());
		}
		setupTinderTab();
	}
	
	
	//////////////////////////////////////////////////////
	//// Tinder Tab /////////////////////////////////////
	//////////////////////////////////////////////////////
	
	private void setupTinderTab() {
		imageViewTinder.setImage(new Image(currentUserProfile.getImgURL()));
		labelUserTinder.setText(currentUserProfile.getFirstname() + ", " + currentUserProfile.getAge());
		
	}
    
    @FXML
    void genderCheck(ActionEvent event) {
    	
    	RadioButton rBtn = (RadioButton) event.getSource();
//    	System.out.println("cu: " + currentUserProfile.getGender().toString().toLowerCase() + ", switch to (btn): " + rBtn.getId() );
    	
    	switch (rBtn.getId()) {
    	case "female": 
    		genderChoice = Gender.FEMALE;
    		break;
    	case "male":
    		genderChoice = Gender.MALE;
    		break;
    	}
    	// wenn Geschlecht des aktuellen Users nicht der Auswahl (RadioButtons) entspricht, dann nächsten Datensatz laden;
    	if (!currentUserProfile.getGender().toString().toLowerCase().equals(rBtn.getId())) loadNextUserProfile();	
    }
    
    @FXML
    void onDislike(ActionEvent event) {
//    	System.out.println("onDislike");
    	// update lokale Userlist der DB
    	userListDB.get(userListDB.indexOf(currentUserProfile)).getLikes().put(activeUser.getId(), LikeState.DISLIKE);
    	// update Userlist der DB auf Server
    	dao.updateLikes(currentUserProfile.getId(), activeUser.getId(), LikeState.DISLIKE);
    	loadNextUserProfile();

    }

    @FXML
    void onLike(ActionEvent event) {
//    	System.out.println("onLike");
    	// update lokale Userlist der DB
    	System.out.println("index of curUser in localDB: " + userListDB.indexOf(currentUserProfile)+ ", ID cuUser: "+ currentUserProfile.getId());  
    	userListDB.get(userListDB.indexOf(currentUserProfile)).getLikes().put(activeUser.getId(), LikeState.LIKE);
    	// update Userlist der DB auf Server
    	dao.updateLikes(currentUserProfile.getId(), activeUser.getId(), LikeState.LIKE);
    	generateMatches();
    	loadNextUserProfile();
    	updateFavTableView();
    }
    
    
	//////////////////////////////////////////////////////
	//// Favourites Tab //////////////////////////////////
	//////////////////////////////////////////////////////
    
    private void updateFavTableView() {
    	ArrayList<User> favUserList = new ArrayList<>();
		for (User user : userListDB) {
			if (user.getLikes().get(activeUser.getId()) == LikeState.LIKE) {
//					System.out.println(user);
//				user.getBtn().setText("Dislike");
				user.getBtn().setGraphic(new ImageView(new Image("img/dislikeBtn_03.png", 27, 27, true, false)));
				user.getBtn().getStyleClass().add("favBtn");
				user.getBtn().setOnAction(ae -> deleteFromFavTableView(user)); // per Dislike-Button FavTableView-Liste bereinigen, siehe auch setupContextMenu mit selber Funktionalität
				favUserList.add(user);
			}
		}
    	favTableView.getItems().setAll(favUserList);
    	favTab.setText("Your Favourites (" + favUserList.size() +")");
	}

	private void setupTableView() {
//		imgCol.setCellValueFactory(new PropertyValueFactory<>("imgURL"));
		imgCol.setCellValueFactory(new PropertyValueFactory<>("img"));  // getImg liefert ImageView
		nameCol.setCellValueFactory(new PropertyValueFactory<>("firstname"));
		ageCol.setCellValueFactory(new PropertyValueFactory<>("age"));
		btnCol.setCellValueFactory(new PropertyValueFactory<>("btn"));
	}
	
	private void setupContextMenu() {
		//Contextmenu mit MenuItem erstellen und der TableView hinzufügen
		ContextMenu cm = new ContextMenu();
		MenuItem dislikeItem = new MenuItem("Dislike");
		cm.getItems().add(dislikeItem);
		favTableView.setContextMenu(cm);
		
		// MenuItem onAction() 
		dislikeItem.setOnAction(e -> {
			User user = favTableView.getSelectionModel().getSelectedItem();
			deleteFromFavTableView(user);				// per Dislike-Kontextmenü FavTableView-Liste bereinigen
		});
	}
	private void deleteFromFavTableView(User user) {
		// update lokale Userlist 
		userListDB.get(userListDB.indexOf(user)).getLikes().put(activeUser.getId(), LikeState.DISLIKE);
		// update userlist auf Server/DB
		Boolean disliked = dao.updateLikes(user.getId(), activeUser.getId(), LikeState.DISLIKE);
		System.out.println("selected Item: " + user.getFirstname() + " disliked: " + disliked);
		if(disliked) {
			updateFavTableView();
			System.out.println("Datensatz geändert: " + user.toString() );
		}
	}
	
	//////////////////////////////////////////////////////
	//// Matches Tab /////////////////////////////////////
	//////////////////////////////////////////////////////
	
	/*
	 * This onSelectionChanged-Method is called when the tab becomes selected and unselected, but I want just method called when the tab becomes selected
	 * FIXED: with if(matchesTab.isSelected()) by Support von Lars 
	 */
	@FXML
	void onMatchesTab(Event event) {
		if(matchesTab.isSelected()) {
//    			System.out.println("Tab is Selected");
			matchesList = new ArrayList<>();
			System.out.println(activeUser.toString());

			for (User user : userListDB) {
				LikeState userLikes = user.getLikes().get(activeUser.getId());  			// Like oder Dislike, das der User vom aktiven Anwender erhalten hat, sonst null
				LikeState activeUserLikes = activeUser.getLikes().get(user.getId());		// Like oder Dislike, das der aktiver Anwender vom User erhalten hat, sonst null
				if (userLikes == LikeState.LIKE && activeUserLikes == LikeState.LIKE) { // Prüfung ob sich User und aktiver Anwender gegenseitig ein "Like" gegeben haben
					System.out.println("Match!");
					matchesList.add(user);
				}
			}
			matchId = 0;
			setupMatchesTab();
		}
	}
	
	@FXML
	void onNext(ActionEvent event) {
		if(matchId < (matchesList.size() - 1)) {
			matchId++;
		} else {
			matchId = 0;
		}
		setupMatchesTab();
	}
	
	private void setupMatchesTab() {
		if(matchesList.isEmpty()) {
			Alert a = new Alert(AlertType.NONE,"Du hast leider kein einziges Match. :(", ButtonType.OK);
			a.show();
			
			imageViewMatches.setImage(new Image("img/noMatches.jpg"));
			labelUserMatches.setText(null);
			
			labelFirstname.setText(null);
		    labelLastname.setText(null);
		    labelStreet.setText(null);
		    labelPostcode.setText(null);
		    labelCity.setText(null);
		    labelEmail.setText(null);
		    labelPhone.setText(null);
			
			labelMatchesCount.setText("NO Matches!");
			btnNext.setVisible(false);
		} else {
			imageViewMatches.setImage(new Image(matchesList.get(matchId).getImgURL()));
			labelUserMatches.setText(matchesList.get(matchId).getFirstname() + ", " + matchesList.get(matchId).getAge());
			
			labelFirstname.setText(matchesList.get(matchId).getFirstname());
		    labelLastname.setText(matchesList.get(matchId).getLastname());
		    labelStreet.setText(matchesList.get(matchId).getStreet());
		    labelPostcode.setText(String.valueOf(matchesList.get(matchId).getPostcode()));
		    labelCity.setText(matchesList.get(matchId).getCity());
		    labelEmail.setText(matchesList.get(matchId).getEmail());
		    labelPhone.setText(matchesList.get(matchId).getPhone());
			
			labelMatchesCount.setText((matchId + 1) + " / " + matchesList.size() + " Matches");
			
			if(matchesList.size() < 2) {
				labelMatchesCount.setText("1 Match");
				btnNext.setVisible(false);
			} else {
				labelMatchesCount.setText((matchId + 1) + " / " + matchesList.size() + " Matches");
				btnNext.setVisible(true);
			}
			
		}
	}
	
	/*
	 * da die random User nicht selbst "liken" können, übernimmt das 
	 * ein Zufallsgenerator
	 */
	private void generateMatches() {
		boolean luck = rg.nextBoolean();
		if(luck) {
			// lokale Userliste und DB updaten
			userListDB.get(userListDB.indexOf(activeUser)).getLikes().put(currentUserProfile.getId(), LikeState.LIKE);
			dao.updateLikes(activeUser.getId(), currentUserProfile.getId(), LikeState.LIKE);
		}
//		System.out.println("Meine erhaltenen Likes: " + activeUser.getLikes().keySet());
	}
	
	
	
	//////////////////////////////////////////////////////
	//// Your Profile Tab ////////////////////////////////
	//////////////////////////////////////////////////////
	
	private void setupProfileTab() {
		imageViewYourProfile.setImage(new Image(activeUser.getImgURL()));
	    labelYourFirstname.setText(activeUser.getFirstname());
	    labelYourLastname.setText(activeUser.getLastname());
	    labelYourStreet.setText(activeUser.getStreet());
	    labelYourPostcode.setText(String.valueOf(activeUser.getPostcode()));
	    labelYourCity.setText(activeUser.getCity());
	    labelYourEmail.setText(activeUser.getEmail());
	    labelYourPhone.setText(activeUser.getPhone());
	}
	
    @FXML
    void onAnother(ActionEvent event) {
    	int newId = rg.nextInt(userListDB.size());
    	activeUser = userListDB.get(newId);
    	setupProfileTab();
    	updateFavTableView();
    }
	
	
	
	//////////////////////////////////////////////////////
	//// errorHandling der Services //////////////////////
	//////////////////////////////////////////////////////
	
//	private void errorHandling() {
//		count.setOnFailed(event -> {
//			System.out.println("DB-Counter-Abfrage ist fehlgeschlagen. " + event.getSource().getException());
//			event.getSource().getException().printStackTrace();
//		});  
//		
//		loadUserList.setOnFailed(event -> {
//			System.out.println("Laden der RandomUserList ist fehlgeschlagen. " + event.getSource().getException());
//			event.getSource().getException().printStackTrace();
//		});
//		
//		saveUserToServer.setOnFailed(event -> {
//			System.out.println("Speichern der RandomUserList in der Datenbank ist fehlgeschlagen. " + event.getSource().getException());
//			event.getSource().getException().printStackTrace();
//		});
//	}
}
