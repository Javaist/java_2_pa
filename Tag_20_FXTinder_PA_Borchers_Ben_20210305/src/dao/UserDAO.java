package dao;

import java.util.List;

import models.LikeState;
import models.User;

public interface UserDAO {
	
	List<User> findAll();
	
	Boolean updateLikes(int id, int newKey, LikeState newValue);
	

}
