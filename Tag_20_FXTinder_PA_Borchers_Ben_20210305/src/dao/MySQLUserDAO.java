package dao;

import java.lang.reflect.Type;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import db.DBConnection;
import javafx.scene.control.Button;
import models.Gender;
import models.LikeState;
import models.User;

public class MySQLUserDAO implements UserDAO{
	private Connection con;
	private Gson gson;
	private Type type;
	
	public MySQLUserDAO() {
		con = DBConnection.getInstance().connection();
		gson = new Gson();
		type = new TypeToken<HashMap<Integer, LikeState>>(){}.getType();
	}
	
	@Override
	public List<User> findAll() {
		ArrayList<User> list = new ArrayList<>();
		
		String sql = "SELECT * FROM userlist";
		try(PreparedStatement ps = con.prepareStatement(sql)){
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				// ResultSet to User-Object
				// Zwischenvariablen für die Werte des Datensatzes
				User user = new User();
				int id = rs.getInt("id");
				Gender gender = Gender.valueOf(rs.getString("gender"));
				String firstname = rs.getString("firstname");
				String lasttname = rs.getString("lastname");
				String street = rs.getString("street");
				String city = rs.getString("city");
				String state = rs.getString("state");
				int postcode = rs.getInt("postcode");
				String email = rs.getString("email");
				String phone = rs.getString("phone");
				LocalDateTime dob = LocalDateTime.parse(rs.getString("dob"));
				String picture = rs.getString("picture");
				
				String json = rs.getString("likes");
				
				HashMap<Integer, LikeState> likesMap = gson.fromJson(json, type);
				
				// Werte dem User-Objekt zuweisen
				user.setId(id);
				user.setGender(gender);
				user.setFirstname(firstname);
				user.setLastname(lasttname); 
				user.setStreet(street);
				user.setCity(city);
				user.setState(state);
				user.setPostcode(postcode);  
				user.setEmail(email); 
				user.setPhone(phone);
				user.setDob(dob);
				user.setImgURL(picture);
				user.setLikes(likesMap);
				user.setBtn(new Button());  // Button for TableView on FavTab
				
				list.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return list;
	}

	@Override
	public Boolean updateLikes(int id, int newKey, LikeState newValue) {
		
		HashMap<Integer, LikeState> likesMap = new HashMap<>();
		
		// JSON – Map mit Likes vom Server laden
		String sqlSelect = "SELECT likes FROM userlist WHERE id = ?";
		try(PreparedStatement ps = con.prepareStatement(sqlSelect)){
			ps.setInt(1, id);
			ResultSet rs = ps.executeQuery();
			rs.next();
			String json = rs.getString("likes");
			likesMap = gson.fromJson(json, type);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		//	JSON – Map mit Likes UPDATEN und auf Server speichern
		String sqlUpdate = "UPDATE userlist SET likes=? WHERE id =?";
		try(PreparedStatement ps = con.prepareStatement(sqlUpdate)) {
			
			// Map für key und 
			likesMap.put(newKey, newValue);
			String json = gson.toJson(likesMap);
			System.out.println("zu speichernde LikesMap: " + json);
			ps.setString(1, json);
			ps.setInt(2, id);
			
			ps.executeUpdate();
			
			return ps.getUpdateCount() == 1; //true wenn ein Datensatz gespeichert
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return false;
	}

}
