package dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javafx.scene.control.Button;
import models.Gender;
import models.LikeState;
import models.User;

public class DummyUserDAO implements UserDAO {

	ArrayList<User> userList = new ArrayList<>();
	
	public DummyUserDAO() {
		// Liste für Test füllen
		userList.add(new User(1, Gender.MALE, "Ben", "Javanist", "Spreegurkenweg 11", "Berlin", "Berlin", 13064, "java_teilnehmer@cimdata.de", "4711-094108320", LocalDateTime.of(LocalDate.of(1989, 11, 9), LocalTime.of(23, 02)), "https://randomuser.me/api/portraits/men/99.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(2, Gender.FEMALE, "Kristin", "Küster", "Am Schlachthof 24", "Unterföring", "Bayern", 88901, "kkueste@postbox.de", "0712-008624218", LocalDateTime.of(LocalDate.of(1975, 2, 22), LocalTime.of(14, 23)), "https://randomuser.me/api/portraits/women/88.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(3, Gender.FEMALE, "Michaela", "Stürmer", "Landwehrkanal 78", "Lübeck", "Schleswig-Holstein", 22054, "katermiau88@mailfree.de", "0415-08722262", LocalDateTime.of(LocalDate.of(1988, 3, 5), LocalTime.of(11, 02)), "https://randomuser.me/api/portraits/women/22.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(4, Gender.MALE, "Kurt", "Krömer", "Spreewald 120", "Berlin", "Berlin", 14064, "kratzbuerste@rbb.de", "0815-08150815", LocalDateTime.of(LocalDate.of(1995, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/38.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(5, Gender.MALE, "Stefan", "Müller", "Hauptstraße 12A", "Kleinmachnow", "Brandenburg", 16012, "stefan.mueller@gxmail.net", "0505-092626262", LocalDateTime.of(LocalDate.of(2000, 12, 31), LocalTime.of(8, 56)), "https://randomuser.me/api/portraits/men/27.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(147, Gender.FEMALE, "Adriana", "Plötz", "Mühlenstraße 7422", "Suhl", "Thüringen", 30079, "adriana.plotz@example.com", "0174-3169063", LocalDateTime.of(LocalDate.of(1945, 12, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/29.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(148, Gender.MALE, "Horst-Günter", "Scheidt", "Grüner Weg 8057", "Waldenburg", "Bayern", 70532, "horst-gunter.scheidt@example.com", "0178-9569647", LocalDateTime.of(LocalDate.of(1995, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/72.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(149, Gender.MALE, "Romuald", "Wilken", "Marktplatz 3053", "Lößnitz", "Rheinland-Pfalz", 65997, "romuald.wilken@example.com", "0179-3927540", LocalDateTime.of(LocalDate.of(1990, 8, 15), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/34.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(150, Gender.MALE, "Edelbert", "Reinartz", "Schulstraße 3017", "Annaberg", "Hamburg", 46921, "edelbert.reinartz@example.com", "0177-4426896", LocalDateTime.of(LocalDate.of(1988, 7, 7), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/12.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(151, Gender.MALE, "Donald", "Hofmann", "Kirchgasse 5836", "Worms", "Baden-Württemberg", 21747, "donald.hofmann@example.com", "0171-5016512", LocalDateTime.of(LocalDate.of(2000, 01, 5), LocalTime.of(11, 13)), "https://randomuser.me/api/portraits/men/88.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(152, Gender.FEMALE, "Hulda", "Sorge", "Kastanienweg 9928", "Brand-Erbisdorf", "Hamburg", 27350, "hulda.sorge@example.com", "0175-4279679", LocalDateTime.of(LocalDate.of(1977, 4, 25), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/7.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(153, Gender.FEMALE, "Janna", "Wanke", "Feldstraße 5182", "Darmstadt", "Bayern", 43071, "janna.wanke@example.com", "0176-7079126", LocalDateTime.of(LocalDate.of(1964, 2, 28), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/96.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(154, Gender.MALE, "Timm", "Nies", "Am Sportplatz 6582", "Berlin", "Rheinland-Pfalz", 16027, "timm.nies@example.com", "0175-1669015", LocalDateTime.of(LocalDate.of(1971, 3, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/77.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(155, Gender.FEMALE, "Sieglinde", "Ehlert", "Friedhofstraße 2102", "Schwerin", "Bayern", 21770, "sieglinde.ehlert@example.com", "0174-0929646", LocalDateTime.of(LocalDate.of(1933, 6, 5), LocalTime.of(20, 10)), "https://randomuser.me/api/portraits/women/3.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(156, Gender.FEMALE, "Gaby", "Storz", "Drosselweg 5458", "Ammerland", "Rheinland-Pfalz", 20409, "gaby.storz@example.com", "0177-7422162", LocalDateTime.of(LocalDate.of(1952, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/53.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(157, Gender.MALE, "Klaus-Michael", "Meiners", "Bergstraße 6336", "Pinneberg", "Berlin", 96909, "klaus-michael.meiners@example.com", "0176-0968856", LocalDateTime.of(LocalDate.of(1963, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/41.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(158, Gender.FEMALE, "Jaqueline", "Kratz", "Hauptstraße 2987", "Berching", "Berlin", 52945, "jaqueline.kratz@example.com", "0170-3290814", LocalDateTime.of(LocalDate.of(1956, 6, 5), LocalTime.of(13, 13)), "https://randomuser.me/api/portraits/women/57.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(159, Gender.FEMALE, "Semra", "Mildner", "Schützenstraße 6659", "Telgte", "Hamburg", 47731, "semra.mildner@example.com", "0174-0416434", LocalDateTime.of(LocalDate.of(1974, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/72.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(160, Gender.FEMALE, "Christel", "Eckardt", "Rosenstraße 4664", "Mühldorf am Inn", "Berlin", 52823, "christel.eckardt@example.com", "0175-5883717", LocalDateTime.of(LocalDate.of(1980, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/27.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(161, Gender.FEMALE, "Carin", "Ehlers", "Kiefernweg 4262", "Thum", "Mecklenburg-Vorpommern", 32616, "carin.ehlers@example.com", "0170-9069206", LocalDateTime.of(LocalDate.of(1981, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/96.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(162, Gender.FEMALE, "Jelena", "Mainka", "Mozartstraße 187", "Waltershausen", "Schleswig-Holstein", 93223, "jelena.mainka@example.com", "0173-9265410", LocalDateTime.of(LocalDate.of(1986, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/51.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(163, Gender.MALE, "Kilian", "Wölk", "Breslauer Straße 4982", "Torgelow", "Mecklenburg-Vorpommern", 28741, "kilian.wolk@example.com", "0178-3817242", LocalDateTime.of(LocalDate.of(1999, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/88.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(164, Gender.FEMALE, "Cläre", "Jahnke", "Mittelstraße 6356", "Netzschkau", "Schleswig-Holstein", 30514, "clare.jahnke@example.com", "0178-4127120", LocalDateTime.of(LocalDate.of(1993, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/70.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(165, Gender.FEMALE, "Heiderose", "Heide", "Blumenstraße 9608", "Ebersberg", "Berlin", 51462, "heiderose.heide@example.com", "0179-1812947", LocalDateTime.of(LocalDate.of(1992, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/64.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(166, Gender.FEMALE, "Milena", "Schewe", "Eichenweg 230", "Bonn", "Baden-Württemberg", 15008, "milena.schewe@example.com", "0174-6003921", LocalDateTime.of(LocalDate.of(1991, 6, 5), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/65.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(167, Gender.FEMALE, "Lia", "Steininger", "Gartenstraße 4283", "Hechingen", "Sachsen-Anhalt", 32326, "lia.steininger@example.com", "0175-6552696", LocalDateTime.of(LocalDate.of(1968, 6, 8), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/32.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(168, Gender.MALE, "Anatoli", "Göhler", "Industriestraße 5573", "Gemünden (Wohra)", "Bremen", 66501, "anatoli.gohler@example.com", "0177-9674647", LocalDateTime.of(LocalDate.of(1996, 10, 3), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/16.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(169, Gender.FEMALE, "Brigitta", "Geßner", "Blumenstraße 7344", "Sinsheim", "Bayern", 57908, "brigitta.gessner@example.com", "0174-1643272", LocalDateTime.of(LocalDate.of(1998, 11, 9), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/women/81.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(170, Gender.MALE, "Sigfried", "Hartl", "Schulstraße 9220", "Röbel/Müritz", "Niedersachsen", 70942, "sigfried.hartl@example.com", "0175-2744687", LocalDateTime.of(LocalDate.of(1995, 12, 12), LocalTime.of(20, 13)), "https://randomuser.me/api/portraits/men/50.jpg", new HashMap<Integer, LikeState>(), new Button()));
		userList.add(new User(171, Gender.MALE, "Theo", "Weiner", "Birkenweg 5875", "Horstmar", "Hessen", 62197, "theo.weiner@example.com", "0177-5947766", LocalDateTime.of(LocalDate.of(2001, 3, 3), LocalTime.of(3, 3)), "https://randomuser.me/api/portraits/men/26.jpg", new HashMap<Integer, LikeState>(), new Button()));
	}

	@Override
	public List<User> findAll() {
		return userList;
	}
	
	@Override
	public Boolean updateLikes(int id, int newKey, LikeState newValue) {
		User findUser = new User();
		for (User user : userList) {
			if(user.getId() == id) {
				findUser = user;
			}
		}
		userList.get(userList.indexOf(findUser)).getLikes().put(newKey, newValue);
		return true;  //.put setzt neu oder überschreibt, daher immer true
	}

}
