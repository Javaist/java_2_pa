package db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

// nur eine Verbindung (DBConnect Instance) zulassen
//final -> verhindert Vererbung
public class DBConnection {

	private Connection con;
	private static DBConnection instance = null;
	
	// Konstruktor private, daher von außen nicht erreichbar, kein Objekt von außen erzeugbar
	private DBConnection() {
		try {
			con = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/java2", "root", "root");
		} catch (SQLException e) {
			System.out.println("Fehler beim Verbinden zur Datenbank: " + e);
		}
	}
	//es wird sichergstellt, dass nur EINE Instanz von DBConnect existiert --> nur EIN Objekt!
	public synchronized static DBConnection getInstance() {
		if ( instance == null) {
			instance = new DBConnection();
		}
		return instance;
	}
	
	public Connection connection() {
		return con;
	}
	
}
